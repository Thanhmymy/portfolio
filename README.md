# Portfolio

Pendant la formation Dev Web et Mobile, nous avons comme projet de faire un portfolio, et de le mettre sur Gitlab.
J'ai décidé de faire portfolio simple, avec des couleurs qui me correspondent. 
Voici la maquette : 

![alt text](Assets/Maquette.png)

**Lien:** https://thanhmymy.gitlab.io/portfolio